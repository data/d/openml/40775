# OpenML dataset: KR-vs-KP-train

https://www.openml.org/d/40775

**WARNING: This dataset is still in preparation.**

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Source:

Database originally generated and described by Alen Shapiro. 

Donor/Coder: 

Rob Holte (holte '@' uottawa.bitnet). 

The database was supplied to Holte by Peter Clark of the Turing Institute in Glasgow (pete '@' turing.ac.uk).


Data Set Information:

The dataset format is described below. Note: the format of this database was modified on 2/26/90 to conform with the format of all the other databases in the UCI repository of machine learning databases.


Attribute Information:

Classes (2): -- White-can-win (&quot;won&quot;) and White-cannot-win (&quot;nowin&quot;). 

I believe that White is deemed to be unable to win if the Black pawn can safely advance. 

Attributes: see Shapiro's book.

#autoxgboost #autoweka

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/40775) of an [OpenML dataset](https://www.openml.org/d/40775). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/40775/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/40775/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/40775/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

